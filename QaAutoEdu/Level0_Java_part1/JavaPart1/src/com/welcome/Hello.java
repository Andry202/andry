package com.welcome;

public class Hello {

    private String name = "";

    public void setupName(String name){
        this.name = name;
    }

    public void welcome(){
        System.out.println("Привет, " + name + " :)");
    }

    public void byebye(){
        System.out.println("Пока, " + name + " :(");
    }
}