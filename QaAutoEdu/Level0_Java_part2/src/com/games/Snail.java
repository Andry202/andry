package com.games;

public class Snail {
    public static int[][] calculateSnail(int size){
        int[][] m = new int[size][size];
        int i;
        int j;
        if (size % 2 == 0) {
            i = (size / 2)-1;
            j = (size / 2)-1;
        }else {
            i = size / 2;
            j = size / 2;
        }
        int min_i = i; int max_i = i;
        int min_j = j; int max_j = j;
        int d = 1;
        for (int a = 1; a <= size * size; a++) {
            m[i][j] = a;
            switch (d) {
                case 0:
                    i += 1;
                    if (i > max_i) {
                        d = 3;
                        max_i = i;
                    }
                    break;
                case 1:
                    j += 1;
                    if (j > max_j) {
                        d = 0;
                        max_j = j;
                    }
                    break;
                case 2:
                    i -= 1;
                    if (i < min_i) {
                        d = 1;
                        min_i = i;
                    }
                    break;
                case 3:
                    j -= 1;
                    if (j < min_j) {
                        d = 2;
                        min_j = j;
                    }
            }
        }

        for (int k = 0; k < size; k++) {
            for (int v = 0; v < size; v++)
                System.out.print(m[k][v] + "\t");
            System.out.println();
        }
        return m;
    }
}